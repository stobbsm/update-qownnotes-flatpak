package main

import (
	"context"
	"crypto/sha256"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"runtime"
	"strings"

	appdata "gitlab.com/stobbsm/update-qownnotes-flatpak/appdataxml"
	flatpak "gitlab.com/stobbsm/update-qownnotes-flatpak/flatpakmanifest"
	ghreleases "gitlab.com/stobbsm/update-qownnotes-flatpak/githubapi"
)

const (
	tmpdir         string = `/tmp/release_update`
	xmlheader      string = `<?xml version="1.0" encoding="UTF-8"?>` + "\n"
	sourceArchives string = "https://download.tuxfamily.org/qownnotes/src/"
	repo           string = `pbek/QOwnNotes`
	archivePrefix  string = `qownnotes-`
	archiveSuffix  string = `.tar.xz`
)

func main() {
	// application arguments
	var appdataxml, flatpakmanifest string

	if len(os.Args) < 3 {
		usage()
		log.Fatal("Not enough arguments")
	}
	appdataxml = os.Args[1]
	flatpakmanifest = os.Args[2]

	ghtoken, ok := os.LookupEnv(`GITHUB_TOKEN`)
	if !ok {
		log.Fatal("GITHUB_TOKEN not set")
	}

	apiclient := ghreleases.NewClient(ghtoken)
	release, err := apiclient.GetLatestRelease(context.Background(), `pbek`, `QOwnNotes`)
	checkErr(err)

	version := strings.TrimPrefix(release.TagName, `v`)

	log.Println("Version:", version)
	file := fmt.Sprintf(`%s%s%s`, archivePrefix, version, archiveSuffix)
	fileurl := sourceArchives + file
	fmt.Printf("Fileurl: %s\n", fileurl)

	fmt.Println("Making workdir: ", tmpdir)
	os.Mkdir(tmpdir, 0777)

	targetFileName := tmpdir + "/" + file
	fmt.Println("Downloading tarball for latest version: ", targetFileName)

	sha256sum, err := download(targetFileName, fileurl)
	checkErr(err)

	fmt.Println("Sha256sum of downloaded file:", sha256sum)

	fmt.Println("Opening appdata file for processing: ", appdataxml)
	f, err := ioutil.ReadFile(appdataxml)

	var AppData appdata.Appdata

	err = xml.Unmarshal(f, &AppData)
	checkErr(err)

	vFound := false
	for _, v := range AppData.Releases {
		log.Printf("Comparing %s to %s", v.Version, version)
		if strings.Compare(v.Version, version) == 0 {
			fmt.Println("Version already found: ", version)
			vFound = true
			break
		}
	}

	if vFound == false {
		AppData.Releases = append([]appdata.Release{{Version: version, Date: release.PublishedAt, Type: "stable"}}, AppData.Releases...)
		xmloutput, err := xml.MarshalIndent(&AppData, "", "  ")
		xmloutput = []byte(xmlheader + string(xmloutput))
		err = ioutil.WriteFile(appdataxml, xmloutput, 0644)
		checkErr(err)

		var Flatpak flatpak.FlatpakManifest
		f, err = ioutil.ReadFile(flatpakmanifest)
		err = json.Unmarshal(f, &Flatpak)

		for _, v := range Flatpak.Modules {
			if v.Name == "QOwnNotes" {
				v.Sources[0] = flatpak.Source{
					Type:   "archive",
					URL:    fileurl,
					SHA256: sha256sum,
				}
			}
		}

		jsonoutput, err := json.MarshalIndent(&Flatpak, "", "  ")
		checkErr(err)
		err = ioutil.WriteFile(flatpakmanifest, jsonoutput, 0644)
		checkErr(err)
	} else {
		fmt.Println("No update to apply.")
	}

	fmt.Println("Cleaning up workdir: ", tmpdir)
	err = os.RemoveAll(tmpdir)
	checkErr(err)
}

func usage() {
	_, filename, _, _ := runtime.Caller(1)
	filename = path.Base(filename)
	fmt.Printf("Usage: %s [appdata file to update] [json manifest to update]\n", filename)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func download(outfile string, infile string) (string, error) {
	resp, err := http.Get(infile)
	if err != nil {
		return "", err
	}
	out, err := os.Create(outfile)
	defer out.Close()
	if err != nil {
		return "", err
	}

	n, err := io.Copy(out, resp.Body)
	if err != nil {
		return "", err
	}
	fmt.Println("Bytes copied:", n)
	resp.Body.Close()
	f, err := os.Open(outfile)
	defer f.Close()
	if err != nil {
		return "", err
	}

	h := sha256.New()

	if _, err = io.Copy(h, f); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h.Sum(nil)), err
}
