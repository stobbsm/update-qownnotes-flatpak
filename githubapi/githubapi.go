package githubapi

import (
	"context"
	"log"

	"github.com/shurcooL/githubv4"
	"golang.org/x/oauth2"
)

type Client struct {
	c     *githubv4.Client
	token oauth2.TokenSource
}

func NewClient(token string) *Client {
	t := oauth2.StaticTokenSource(
		&oauth2.Token{
			AccessToken: token,
		},
	)
	httpClient := oauth2.NewClient(context.Background(), t)
	c := githubv4.NewClient(httpClient)

	return &Client{c, t}
}

type Release struct {
	PublishedAt string
	TagName     string
}

func (c *Client) GetLatestRelease(ctx context.Context, owner, name string) (Release, error) {
	var query struct {
		Repository struct {
			Releases struct {
				Nodes []Release
			} `graphql:"releases(first:1,orderBy:{field:CREATED_AT,direction:DESC})"`
		} `graphql:"repository(owner:$owner,name:$name)"`
	}

	args := map[string]interface{}{
		"owner": githubv4.String(owner),
		"name":  githubv4.String(name),
	}

	err := c.c.Query(ctx, &query, args)
	if err != nil {
		log.Printf("Result: %v", query)
		return Release{}, err
	}
	return query.Repository.Releases.Nodes[0], nil
}
