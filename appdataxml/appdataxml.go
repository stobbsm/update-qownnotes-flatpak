package appdataxml

import "encoding/xml"

// Appdata parses and modifies Appdata files
type Appdata struct {
	XMLName         xml.Name     `xml:"component"`
	Type            string       `xml:"type,attr"`
	ID              string       `xml:"id"`
	MetadataLicense string       `xml:"metadata_license"`
	ProjectLicense  string       `xml:"project_license"`
	Name            string       `xml:"name"`
	Summary         string       `xml:"summary"`
	Description     []string     `xml:"description>p"`
	Screenshots     []Screenshot `xml:"screenshots>screenshot"`
	URL             struct {
		Type  string `xml:"type,attr"`
		Value string `xml:",chardata"`
	} `xml:"url"`
	ContentRating ContentRating `xml:"content_rating"`
	Releases      []Release     `xml:"releases>release"`
}

// Screenshot holds the data for Screenshot info
type Screenshot struct {
	Type    string `xml:"type,attr"`
	Caption string `xml:"caption"`
	Image   string `xml:"image"`
}

// ContentRating is part of the spec
type ContentRating struct {
	Type              string             `xml:"type,attr"`
	ContentAttributes []ContentAttribute `xml:"content_attribute"`
}

// ContentAttribute belongs to the ContentRating section
type ContentAttribute struct {
	ID    string `xml:"id,attr"`
	Value string `xml:",chardata"`
}

// Release is a standard release in the Releases section
type Release struct {
	Version string `xml:"version,attr"`
	Date    string `xml:"date,attr"`
	Type    string `xml:"type,attr"`
}
