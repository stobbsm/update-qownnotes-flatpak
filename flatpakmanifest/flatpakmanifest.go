package flatpakmanifest

// FlatpakManifest is the structure to contain a json flatpak manifest
type FlatpakManifest struct {
	AppID             string   `json:"app-id"`
	Runtime           string   `json:"runtime"`
	RuntimeVersion    string   `json:"runtime-version"`
	Sdk               string   `json:"sdk"`
	Command           string   `json:"command"`
	RenameDesktopFile string   `json:"rename-desktop-file,omitempty"`
	RenameIcon        string   `json:"rename-icon,omitempty"`
	FinishArgs        []string `json:"finish-args,omitempty"`
	Modules           []Module `json:"modules"`
}

// Module is the module internal structure
type Module struct {
	Name          string   `json:"name"`
	BuildSystem   string   `json:"buildsystem,omitempty"`
	BuildCommands []string `json:"build-commands,omitempty"`
	Subdir        string   `json:"subdir,omitempty"`
	MakeArgs []string `json:"make-args,omitempty"`
	MakeInstallArgs []string `json:"make-install-args,omitempty"`
	Sources       []Source `json:"sources,omitempty"`
}

// Source is the module source internal structure
type Source struct {
	Type   string `json:"type,omitempty"`
	URL    string `json:"url,omitempty"`
	Tag    string `json:"tag,omitempty"`
	Commit string `json:"commit,omitempty"`
	Path   string `json:"path,omitempty"`
	SHA256 string `json:"sha256,omitempty"`
}
