module gitlab.com/stobbsm/update-qownnotes-flatpak

go 1.13

require (
	github.com/Masterminds/semver v1.5.0
	github.com/shurcooL/githubv4 v0.0.0-20201206200315-234843c633fa
	github.com/shurcooL/graphql v0.0.0-20200928012149-18c5c3165e3a // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	golang.org/x/oauth2 v0.0.0-20201208152858-08078c50e5b5
)
